import { Component, Inject, Type } from '@angular/core';
import { Platform, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavigationEnd, NavigationStart, RouteConfigLoadStart, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public router: Router,
    public loader: LoadingController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.onRouter();
    });
  }

  private async onRouter() {
    // const loader = await this.loader.create();

    // this.router.events.subscribe(e => {
    //   if (e instanceof NavigationStart || e instanceof RouteConfigLoadStart) {
    //     loader.present();
    //   } else if (e instanceof NavigationEnd) {
    //     this.loader.dismiss();
    //   }
    // });
  }


}

