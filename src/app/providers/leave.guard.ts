// import { Injectable } from '@angular/core';
// import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
// import { ModalController } from '@ionic/angular';
// import { MenuComponent } from '@pages/tienda/menu/menu.component';
// import { ModalPopupComponent } from '@shared/components/modal-popup/modal-popup.component';
// import { Observable } from 'rxjs';

// @Injectable({
//     providedIn: 'root'
// })
// export class LeaveGuard implements CanDeactivate<MenuComponent> {

//     constructor(
//         public service: PedidoService,
//         public modal: ModalController
//     ) { }

//     async canDeactivate(
//         component: MenuComponent,
//         route: ActivatedRouteSnapshot,
//         state: RouterStateSnapshot
//     ): Promise<boolean> {

//         const top = await this.modal.getTop();

//         //
//         // -- Android back button
//         // De estar el modal abierto no permite salir o cerrar dicho modal
//         //
//         if (top) {
//             return false;
//         }

//         if (this.service.pedidos.length < 1 || this.service.canGoToPedido) {
//             this.service.canGoToPedido = false;
//             return true;
//         }

//         const modal = await this.modal.create({
//             component: ModalPopupComponent,
//             cssClass: 'modal-popup',
//             componentProps: {
//                 title: 'Salir del Restaurante',
//                 message: '¿Estas seguro que quieres salir? Pedidos seleccionados seran borrados.'
//             }
//         });

//         await modal.present();

//         const { data } = await modal.onDidDismiss();

//         if (!data?.dismissed) {
//             this.service.pedidos = [];
//             return true;
//         }

//         return false;
//     }
// }
