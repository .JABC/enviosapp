import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { IMenu } from '@pages/tienda/menu/menu.model';
import { IPedido, Pedido } from '@pages/tienda/pedido/pedido.model';

@Injectable({providedIn: 'root'})
export class MenuService {
    
    editing = false;
    waiting = true;
    data: IMenu[] = [];
    model: IMenu = { };
    readonly pedido: Pedido = new Pedido();

    constructor(
        public http: HttpClient,
        public loader: LoadingController,
        public router: Router,
        public route: ActivatedRoute
    ) { }

    async load(id: number): Promise<any> {
        this.waiting = true;
        const { data } = await this.http.get('assets/data/data.json').toPromise() as any;
        this.data = (data[0]?.shops as any[]).find(x => x.id === id)?.menu;
        console.log(this.data);
        this.waiting = false;

        return Promise.resolve(data);
    }

    onAdd(model?: any) {
        const item = model || this.model;
        //
        if (!this.editing) {
            this.pedido.add(item);
        } else {
            this.pedido.edit(item);
            this.editing = false;
        }
        //
        this.router.navigate([this.route.snapshot.queryParams["returnUrl"]]);
    }

    onDelete(model: IPedido) {
        const item = model || this.model;
        //
        this.pedido.delete(item);
        //
        if (this.pedido.pedidos.length < 1) {
            this.router.navigate(['/app/menu']);
        }
    }

}