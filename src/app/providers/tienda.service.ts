import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EShopState, ITienda } from '@pages/tienda/tienda.model';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class TiendaService {

    waiting = false;
    // Change type later
    data: { shops: ITienda[], categories: any[] }[] = [];
    model: ITienda = { };

    constructor(
        public http: HttpClient,
        public router: Router
        // public loader: LoadingController
    ) { }

    async load(): Promise<any> {
        this.waiting = true;

        const { data } = await this.http.get('assets/data/data.json').toPromise() as any;
        this.data = data;

        this.waiting = false;

        return Promise.resolve(data);
    }

    onMenu(model: any) {
        if (model.state == EShopState.Closed ||
            model.state == EShopState.ClosedTemporarily) {
            return;
        }

        this.model = model;
        this.router.navigate(['/app/menu']);
    }

    onCategory(model: any) {
        const encode = btoa(JSON.stringify(model)) + '==';
        this.router.navigate(['/app/categoria/list'], {
            queryParams: {
                key: encode
            }
        });
    }

    get promo(): any[] {
        return this.data[0]?.shops.filter(x => x.hasPromo) as any[];
    }

}
