import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ConfiguracionComponent } from './configuracion.component';
import { SharedModule } from '@shared/shared.module';

const routes: Routes = [
    {  path: '', redirectTo: 'opciones', pathMatch: 'full'},
    {
        path: '',
        children: [
            {
                path: '',
                component: ConfiguracionComponent
            }
        ]
    }
];

@NgModule({
    declarations: [
        ConfiguracionComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: []
})
export class ConfiguracionModule { }
