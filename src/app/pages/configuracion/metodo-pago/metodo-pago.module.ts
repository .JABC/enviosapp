import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { MetodoPagoComponent } from './metodo-pago.component';
import { MetodoPagoFormComponent } from './form/form.component';

const routes: Routes = [
    {
        path: '',
        component: MetodoPagoComponent
    },
    {
        path: 'form',
        component: MetodoPagoFormComponent
    }
];

@NgModule({
    declarations: [
        MetodoPagoComponent,
        MetodoPagoFormComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class MetodoPagoModule { }
