import { Component, OnInit } from '@angular/core';
import { IonRouterOutlet, ModalController } from '@ionic/angular';

@Component({
    selector: 'app-ajustes',
    templateUrl: './ajustes.component.html'
})
export class AjustesComponent implements OnInit {

    model: any = { };

    constructor(
        public modal: ModalController,
        public outlet: IonRouterOutlet
    ) { }

    ngOnInit(): void { }

}
