import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { AjustesComponent } from './ajustes.component';
import { NotificacionesComponent } from './notificaciones/notificaciones.component';
import { TerminosComponent } from './terminos/terminos.component';

const routes: Routes = [
    {
        path: '',
        component: AjustesComponent
    },
    {
        path: 'notificaciones',
        component: NotificacionesComponent
    },
    {
        path: 'terminos',
        component: TerminosComponent
    }
];

@NgModule({
    declarations: [
        AjustesComponent,
        TerminosComponent,
        NotificacionesComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class AjustesModule { }
