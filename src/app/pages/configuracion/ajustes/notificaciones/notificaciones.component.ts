import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-notificaciones',
    templateUrl: './notificaciones.component.html'
})
export class NotificacionesComponent implements OnInit {

    constructor(
        public modal: ModalController
    ) { }

    ngOnInit(): void { }
}
