import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-terminos',
    templateUrl: './terminos.component.html'
})
export class TerminosComponent implements OnInit {

    constructor(
        public modal: ModalController
    ) { }

    ngOnInit(): void { }
}
