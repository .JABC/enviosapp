import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { NosotrosComponent } from './nosotros.component';

const routes: Routes = [
    {  path: '', redirectTo: 'opciones', pathMatch: 'full'},
    {
        path: '',
        children: [
            {
                path: '',
                component: NosotrosComponent
            }
        ]
    }
];

@NgModule({
    declarations: [
        NosotrosComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: []
})
export class NosotrosModule { }
