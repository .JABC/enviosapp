import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { FavoritosComponent } from './favoritos.component';

const routes: Routes = [
    {
        path: '',
        component: FavoritosComponent
    }
]

@NgModule({
    declarations: [
        FavoritosComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class FavoritosModule { }
