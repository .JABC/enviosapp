import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-favoritos',
    templateUrl: './favoritos.component.html'
})
export class FavoritosComponent implements OnInit {

    items = [1, 2, 3, 4, 5, 6, 7];

    constructor() { }

    ngOnInit(): void { }

}
