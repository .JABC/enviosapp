import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FADE_ANIMATION } from '@shared/utils/animations';

@Component({
    selector: 'app-cuenta',
    templateUrl: './cuenta.component.html',
    animations: [FADE_ANIMATION]
})
export class CuentaComponent implements OnInit {

    canEdit = false;

    constructor(
        public modal: ModalController
    ) { }

    ngOnInit(): void { }

}
