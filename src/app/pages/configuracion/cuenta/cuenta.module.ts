import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { CuentaComponent } from './cuenta.component';
import { ContrasenaComponent } from './contrasena/contrasena.component';

const routes: Routes = [
    {
        path: '',
        component: CuentaComponent
    },
    {
        path: 'contrasena',
        component: ContrasenaComponent
    }
];

@NgModule({
    declarations: [
        CuentaComponent,
        ContrasenaComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class CuentaModule { }
