import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-historial-detail',
    templateUrl: './historial-detail.component.html'
})
export class HistorialDetailComponent implements OnInit {

    @Output() list = new EventEmitter();

    @Output() reorder = new EventEmitter();

    constructor() { }

    ngOnInit(): void { }
}
