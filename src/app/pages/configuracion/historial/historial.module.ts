import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { HistorialComponent } from './historial.component';
import { HistorialDetailComponent } from './components/historial-detail.component';
import { HistorialListComponent } from './list/list.component';

const routes: Routes = [
    {
        path: '',
        component: HistorialComponent
    },
    {
        path: 'list',
        component: HistorialListComponent
    }
];

@NgModule({
    declarations: [
        HistorialComponent,
        HistorialDetailComponent,
        HistorialListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: []
})
export class HistorialModule { }
