import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-historial',
    templateUrl: './historial.component.html'
})
export class HistorialComponent implements OnInit {

    items: any[] = [1, 2, 3, 4]

    constructor(
        public router: Router
    ) { }

    ngOnInit(): void { }

    reorder() {
        alert('Reordenar');
    }

}
