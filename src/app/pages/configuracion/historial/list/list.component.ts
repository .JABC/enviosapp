import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-list-historial',
    templateUrl: './list.component.html'
})
export class HistorialListComponent implements OnInit {

    items: any[] = [1, 2, 3, 4, 5, 6, 7, 8];

    constructor() { }

    ngOnInit(): void { }
}
