import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio.component';
import { SharedModule } from '@shared/shared.module';

const routes: Routes = [
    {
        path: '',
        component: InicioComponent
    }
]

@NgModule({
    declarations: [
        InicioComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class InicioModule { }
