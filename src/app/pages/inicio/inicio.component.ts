import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { TiendaService } from '@providers/tienda.service';

@Component({
    selector: 'app-inicio',
    templateUrl: './inicio.component.html'
})
export class InicioComponent implements OnInit {

    offers = [1, 2];

    items = [1, 2, 3];

    constructor(
        public modal: ModalController,
        public service: TiendaService
    ) { }

    ngOnInit(): void {
        this.service.load();
    }

    alert(e) {
        alert(e);
    }

}
