import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { BusquedaComponent } from './Busqueda.component';

const routes: Routes = [
    {
        path: '',
        component: BusquedaComponent
    }
];

@NgModule({
    declarations: [
        BusquedaComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: []
})
export class BusquedaModule { }
