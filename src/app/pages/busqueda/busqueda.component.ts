import { Component, OnInit } from '@angular/core';
import { TiendaService } from '@providers/tienda.service';

@Component({
    selector: 'app-busqueda',
    templateUrl: './busqueda.component.html'
})
export class BusquedaComponent implements OnInit { 

    data: any[] = [];

    hasSearched = false;

    recents: string[] = ["Burger King", "KFC"];

    constructor(
        public readonly service: TiendaService
    ) { }

    ngOnInit(): void { }

    async research(value: string) {
        if (!value || value?.trim() === '') {
            return;
        }

        const data = await this.service.load();
        this.data = data[0]?.shops.filter(x => {
            return (x.name as string)
                .toLowerCase()
                .includes(String(value).toLowerCase())
        });
        
        // must have one 'research' at least to hide recents' section
        this.hasSearched = true;
    }

}
