import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { SlidesComponent } from './slides.component';

const routes: Routes = [
    {
        path: '',
        component: SlidesComponent
    }
]

@NgModule({
    declarations: [
        SlidesComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class SlidesModule { }
