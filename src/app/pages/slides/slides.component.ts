import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
    selector: 'app-slides',
    templateUrl: './slides.component.html'
})
export class SlidesComponent implements OnInit {

    activeSlide = 0;

    constructor(
        public router: Router
    ) { }

    ngOnInit(): void { }

    async change(slides: IonSlides) {
        const index = await slides.getActiveIndex();
        this.activeSlide = index;
    }

    skip() {
        this.router.navigate(['/']);
    }

}
