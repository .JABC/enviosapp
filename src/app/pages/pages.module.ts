import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { IonicModule } from '@ionic/angular';
import { TiendaService } from '@providers/tienda.service';
// import { LeaveGuard } from '@providers/leave.guard';
import { MenuService } from '@providers/menu.service';

const routes: Routes = [
    { path: '', redirectTo: 'inicio', pathMatch: 'full' },
    {
        path: '',
        component: PagesComponent,
        children: [
            {
                path: 'inicio',
                loadChildren: () => import('./inicio/inicio.module').then(m => m.InicioModule)
            },
            {
                path: 'tienda',
                loadChildren: () => import('./tienda/tienda.module').then(m => m.TiendaModule)
            },
            {
                path: 'configuracion',
                loadChildren: () => import('./configuracion/configuracion.module').then(m => m.ConfiguracionModule)
            }
        ]
    },
    //
    // Agregar aqui rutas que no integran tabs
    //
    {
        path: 'busqueda',
        loadChildren: () => import('./busqueda/busqueda.module').then(m => m.BusquedaModule)
    },
    //
    // ============= Tienda Tab ==================
    //
    {
        path: 'menu',
        loadChildren: () => import('./tienda/menu/menu.module').then(m => m.MenuModule),
        // canDeactivate: [LeaveGuard]
    },
    {
        path: 'pedido',
        loadChildren: () => import('./tienda/pedido/pedido.module').then(m => m.PedidoModule)
    },
    {
        path: 'checkout',
        loadChildren: () => import('./tienda/checkout/checkout.module').then(m => m.CheckoutModule)
    },
    {
        path: 'filtro',
        loadChildren: () => import('./tienda/filtro/filtro.module').then(m => m.FiltroModule)
    },
    {
        path: 'categoria',
        loadChildren: () => import('./tienda/categoria/categoria.module').then(m => m.CategoriaModule)
    },
    //
    // ============ Configuracion Tab ===============
    //
    {
        path: 'cuenta',
        loadChildren: () => import('./configuracion/cuenta/cuenta.module').then(m => m.CuentaModule)
    },
    {
        path: 'ajustes',
        loadChildren: () => import('./configuracion/ajustes/ajustes.module').then(m => m.AjustesModule)
    },
    {
        path: 'pago',
        loadChildren: () => import('./configuracion/metodo-pago/metodo-pago.module').then(m => m.MetodoPagoModule)
    },
    {
        path: 'historial',
        loadChildren: () => import('./configuracion/historial/historial.module').then(m => m.HistorialModule)
    },
    {
        path: 'favoritos',
        loadChildren: () => import('./configuracion/favoritos/favoritos.module').then(m => m.FavoritosModule)
    },
    {
        path: 'nosotros',
        loadChildren: () => import('./configuracion/nosotros/nosotros.module').then(m => m.NosotrosModule)
    }
];

@NgModule({
    declarations: [
        PagesComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [
        TiendaService,
        MenuService,
        // LeaveGuard
    ],
})
export class PagesModule { }
