import { Component, OnInit } from '@angular/core';
import { TiendaService } from '@providers/tienda.service';
import { Router } from '@angular/router';

@Component({
    templateUrl: './tienda.component.html'
})
export class TiendaComponent implements OnInit {

    constructor(
        public service: TiendaService,
        public router: Router
    ) { }

    ngOnInit(): void {
        this.service.load();
    }
}
