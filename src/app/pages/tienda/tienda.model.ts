import { IMenu } from "./menu/menu.model";

export interface ITienda {
    id?: number;
    name?: string;
    subtitulo?: string;
    rating?: string | number;
    imgURL?: string;
    thumbnail?: string;
    isNew?: boolean;
    state?: EShopState;
    delivery?: string;
    minPrice?: number;
    hasPromo?: boolean;
    categories?: string[];
    menu?: IMenu[];
    comments?: IComment[];
    location?: ILocation;
    acceptDelivery?: boolean;
    acceptPickup?: boolean;
    creditCards?: ECreditCard[];
}

export interface IComment {
    userName?: string;
    userThumbnail?: string;
    message?: string;
    rating?: string | number;
    date?: Date | string;
}

export interface ILocation {
    address?: string;
    lon?: number;
    lat?: number;
}

export enum ECreditCard {
    MasterCard = 1,
    VisaCard
}

export enum EDeliveryType {
    Delivery = 1,
    Pickup
}

export enum EShopState {
    Closed = 1,
    ClosedTemporarily,
    Open
}
