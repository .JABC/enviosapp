import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TiendaService } from '@providers/tienda.service';

@Component({
    selector: 'app-comentario',
    templateUrl: './comentario.component.html'
})
export class ComentarioComponent implements OnInit {

    items = [1, 2, 3, 4, 5];

    constructor(
        public service: TiendaService
    ) { }

    ngOnInit(): void { }

}
