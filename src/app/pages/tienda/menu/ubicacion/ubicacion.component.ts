import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TiendaService } from '@providers/tienda.service';

// declare var google;

@Component({
    selector: 'app-ubicacion',
    templateUrl: './ubicacion.component.html'
})
export class UbicacionComponent implements OnInit {

    @ViewChild('map') mapChild: ElementRef<HTMLElement>;
    mapRef = null;
    
    constructor(
        public service: TiendaService
    ) { }

    ngOnInit(): void {
        // this.loadMap();
    }

    // loadMap() {
    //     this.mapRef = new google.maps.Map(this.mapChild, {
    //         center: { lat: 0, lng: 0 },
    //         zoom: 1
    //     });
    // }

}
