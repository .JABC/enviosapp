import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuService } from '@providers/menu.service';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html'
})
export class FormComponent implements OnInit, OnDestroy {

    constructor(
        public service: MenuService
    ) { }

    ngOnInit(): void {

    }

    // Testing
    alert(e) {
        alert(JSON.stringify(e, null, '\t'));
    }

    ngOnDestroy() {

    }

}
