import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ECreditCard } from '@pages/tienda/tienda.model';
import { TiendaService } from '@providers/tienda.service';

@Component({
    selector: 'app-detalles',
    templateUrl: './detalles.component.html'
})
export class DetallesComponent implements OnInit {

    CREDITCARDTYPE = ECreditCard;

    currentDay = new Date().getDay();

    days = [
        'Lunes',
        'Martes',
        'Miercoles',
        'Jueves',
        'Viernes',
        'Sabado',
        'Domingo'
    ];

    constructor(
        public service: TiendaService
    ) {

    }

    ngOnInit(): void {
        console.log(this.service.model);
    }
}
