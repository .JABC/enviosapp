import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MenuComponent } from './menu.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormComponent } from './form/form.component';
import { ComentarioComponent } from './comentario/comentario.component';
import { DetallesComponent } from './detalles/detalles.component';
import { UbicacionComponent } from './ubicacion/ubicacion.component';

const routes: Routes = [
    {
        path: '',
        component: MenuComponent
    },
    {
        path: 'form',
        component: FormComponent
    },
    {
        path: 'detalles',
        component: DetallesComponent
    },
    {
        path: 'comentario',
        component: ComentarioComponent
    },
    {
        path: 'ubicacion',
        component: UbicacionComponent
    }
];

@NgModule({
    declarations: [
        MenuComponent,
        FormComponent,
        ComentarioComponent,
        DetallesComponent,
        UbicacionComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class MenuModule { }
