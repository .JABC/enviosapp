import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from '@providers/menu.service';
import { TiendaService } from '@providers/tienda.service';
import { SLIDEDOWN_ANIMATION } from '@shared/utils/animations';
import { EMenuState } from './menu.model';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    animations: [SLIDEDOWN_ANIMATION]
})
export class MenuComponent implements OnInit, OnDestroy {

    // @HostListener('ionBackButton', ['$event'])
    // onBackButton(e: Event) {
    //     e.stopPropagation();
    //     e.preventDefault();
    //     this.onBack();
    // }

    options = [
        { label: "Comentarios", icon: "chatbox-outline", route: "/app/menu/comentario" },
        { label: "Detalles", icon: "reader-outline", route: "/app/menu/detalles" },
        { label: "Ubicacion", icon: "map-outline", route: "/app/menu/ubicacion" }
    ];

    constructor(
        // public modal: ModalController,
        public readonly tiendaService: TiendaService,
        public readonly service: MenuService,
        public readonly router: Router
    ) { }

    ngOnInit(): void {
        this.service.load(this.tiendaService.model.id);
    }

    onAdd(model: any) {
        if (model.state === EMenuState.NotAvailable) {
            return;
        }

        this.service.model = Object.assign({}, model);
        this.router.navigate(['/app/menu/form'], {
            queryParams: {
                returnUrl: this.router.routerState.snapshot.url
            }
        });
    }

    // async onBack() {

    //     if (this.service.pedido.pedidos.length < 1) {
    //         history.back();
    //         return;
    //     }

    //     const modal = await this.modal.create({
    //         component: ModalPopupComponent,
    //         cssClass: 'modal-popup',
    //         componentProps: {
    //             title: 'Salir del Restaurante',
    //             message: '¿Estas seguro que quieres salir? Pedidos seleccionados seran borrados.'
    //         }
    //     });

    //     await modal.present();

    //     const { data } = await modal.onDidDismiss();

    //     if (!data?.dismissed) {
    //         history.back();
    //         this.service.pedido.clear();
    //     }

    //     return;
    // }
    
    alert(e) {
        console.log(e);
    }

    ngOnDestroy() {
        this.service.pedido.clear();
    }

}
