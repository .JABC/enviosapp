
export interface IMenu {
    id?: number,
    name?: string,
    details?: string,
    price?: number,
    thumbnail?: string,
    state?: EMenuState,
    rating?: number | string,
    storeName?: string
}

export enum EMenuState {
    MostValuate = 1,
    NotAvailable,
    Available
}
