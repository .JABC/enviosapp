import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TiendaService } from '@providers/tienda.service';

@Component({
    selector: 'app-categoria-list',
    templateUrl: 'list.component.html',

})

export class CategoriaListComponent implements OnInit {

    categoria = null;
    data: any[] = [];

    constructor(
        public service: TiendaService,
        public route: ActivatedRoute,
    ) { }

    ngOnInit() {
        let key = this.route.snapshot.queryParams["key"] as string;
        key = key.substring(0, key.length - 2);
        const decode = JSON.parse(atob(key));
        this.categoria = decode;
        // this.data = this.service.data.filter(y => (y?.categories as any[]));
        this.data = this.service.data[0]?.shops.filter(
            x => (x?.categories as any[])
                .findIndex(y => y.name === this.categoria.name) > -1);
    }
}