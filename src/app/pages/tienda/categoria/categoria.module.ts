import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { CategoriaComponent } from './categoria.component';
import { CategoriaListComponent } from './list/list.component';

const routes: Routes = [
    {
        path: '',
        component: CategoriaComponent
    },
    {
        path: 'list',
        component: CategoriaListComponent
    }
];

@NgModule({
    declarations: [
        CategoriaComponent,
        CategoriaListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class CategoriaModule { }
