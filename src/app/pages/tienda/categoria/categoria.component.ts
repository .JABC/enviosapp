import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TiendaService } from '@providers/tienda.service';

@Component({
    selector: 'app-categoria',
    templateUrl: 'categoria.component.html'
})

export class CategoriaComponent implements OnInit {

    constructor(
        public router: Router,
        public service: TiendaService
    ) { }

    ngOnInit() {

    }
}