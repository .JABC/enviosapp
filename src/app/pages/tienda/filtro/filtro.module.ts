import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { FiltroComponent } from './filtro.component';
import { FiltroListComponent } from './list/list.component';

const routes: Routes = [
    {
        path: '',
        component: FiltroComponent
    },
    {
        path: 'list',
        component: FiltroListComponent
    }
];

@NgModule({
    declarations: [
        FiltroComponent,
        FiltroListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class FiltroModule { }
