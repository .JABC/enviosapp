import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'app-filtro',
    templateUrl: './filtro.component.html'
})
export class FiltroComponent implements OnInit {

    model: any = { };
    filtro: any = { };

    ordernar = [
        { label: "Mas Cercano", value: "cerca" },
        { label: "Mas Valorado", value: "valorado" },
        { label: "Otro", value: "otro" },
        { label: "Otro2", value: "otro2" }
    ];

    entregar = [
        { label: "Delivery", value: "delivery" },
        { label: "Pickup", value: "pickup" }
    ];

    precio = [
        { label: "$", value: "Mbarato" },
        { label: "$$", value: "barato" },
        { label: "$$$", value: "costoso" },
        { label: "$$$$", value: "Mcostoso" }
    ]

    constructor(
        public modal: ModalController,
        public router: Router
    ) { }

    ngOnInit(): void { }

    alert() {
        this.router.navigate(['/app/filtro/list']);
    }

    clear() {
        this.filtro = { };
    }

}
