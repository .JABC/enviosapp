import { Component, OnInit } from '@angular/core';
import { TiendaService } from '@providers/tienda.service';

@Component({
    selector: 'app-filtro-list',
    templateUrl: 'list.component.html'
})

export class FiltroListComponent implements OnInit {
    constructor(public service: TiendaService) { }

    ngOnInit() { }
}