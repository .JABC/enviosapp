import { Component, HostBinding, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MenuService } from '@providers/menu.service';
import { TiendaService } from '@providers/tienda.service';

@Component({
    selector: 'app-factura',
    templateUrl: './factura.component.html'
})
export class FacturaComponent implements OnInit {

    constructor(
        public tiendaService: TiendaService,
        public service: MenuService
    ) { }

    ngOnInit(): void { }

}
