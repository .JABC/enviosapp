import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "@shared/shared.module";
import { CheckoutComponent } from "./checkout.component";
import { FacturaComponent } from "./factura/factura.component";
import { PagoComponent } from "./pago/pago.component";

const routes: Routes = [
    {
        path: '',
        component: CheckoutComponent,
        children: [
            { path: '', redirectTo: 'pago', pathMatch: 'full' },
            {
                path: 'pago',
                component: PagoComponent
            },
            {
                path: 'factura',
                component: FacturaComponent
            }
        ]
    }
];

@NgModule({
    declarations: [
        CheckoutComponent,
        PagoComponent,
        FacturaComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class CheckoutModule { }
