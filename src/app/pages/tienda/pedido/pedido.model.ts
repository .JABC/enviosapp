

export interface IPedido {
    id?: number;
    name?: string;
    details?: string;
    price?: number;
    unit?: number;
    description?: string;
    thumbnail?: string;
}

export class Pedido {

    private _pedidos: IPedido[] = [];

    get pedidos(): IPedido[] {
        return this._pedidos;
    }

    constructor() { }

    add(item: IPedido) {
        const index = this._pedidos.findIndex(x => x.id === item?.id);

        if (index > -1) {
            item.unit += this._pedidos[index].unit;
            this._pedidos.splice(index, 1, item);
            return;
        }
        
        this._pedidos.unshift(item);
    }

    edit(item: IPedido) {
        const index = this._pedidos.findIndex(x => x.id === item?.id);

        if (index > -1) {
            if (item === this._pedidos[index]) {
                return;
            }

            this._pedidos.splice(index, 1, item);
        }
    }

    delete(item: IPedido) {
        const index = this._pedidos.findIndex(x => x.id === item?.id);

        if (index > -1) {
            this._pedidos.splice(index, 1);
        }

    }

    clear() {
        this._pedidos = [];
    }

    subtotal(): number {
        try {
            let total = 0;
            this._pedidos.forEach(item => {
                total += item?.price * item?.unit;
            });

            return total;
        } catch {
            return 0;
        }
    }

    count(): number {
        try {
            const count = (this._pedidos as any[]).reduce((prev, next) => ({ unit: prev?.unit + next?.unit }));
            return count.unit;
        } catch {
            return 0;
        }
    }

}
