import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SharedModule } from "@shared/shared.module";
import { PedidoComponent } from "./pedido.component";

const routes: Routes = [
    {
        path: '',
        component: PedidoComponent
    }
];

@NgModule({
    declarations: [
        PedidoComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class PedidoModule { }
