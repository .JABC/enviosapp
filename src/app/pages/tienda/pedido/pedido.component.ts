import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, PopoverController } from '@ionic/angular';
import { MenuService } from '@providers/menu.service';
import { TiendaService } from '@providers/tienda.service';
import { IPedido } from './pedido.model';

@Component({
    selector: 'app-pedido',
    templateUrl: './pedido.component.html'
})
export class PedidoComponent implements OnInit {

    constructor(
        public service: MenuService,
        public tiendaService: TiendaService,
        public router: Router
    ) { }

    ngOnInit(): void { }

    onAdd(model: any) {
        this.service.model = Object.assign({}, model);
        this.service.editing = true;
        this.router.navigate(['/app/menu/form'], {
            queryParams: {
                returnUrl: this.router.routerState.snapshot.url
            }
        });
    }

    // async presentPedidoPopup() {
    //     const popover = await this.popoverCtrl.create({
    //       component: PedidoPopupComponent
    //     });
    //     return await popover.present();
    // }

    // async editPedido(model: any) {
    //     const modal = await this.modal.create({
    //         component: FormComponent,
    //         componentProps: {
    //             model,
    //             editing: true
    //         }
    //     });

    //     return await modal.present();
    // }

    deletePedido(model: any) {
        // this.service.deletePedido(model);

        // if (this.service.pedidos.length < 1) {
        //     history.back();
        // }

    }

    // Testing
    alert(e) {
        alert(JSON.stringify(e, null, '\t'));
    }

}
