import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'app', pathMatch: 'full' },
  {
    path: 'auth',
    loadChildren: () => import('./pages/auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'app',
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
  },
  {
    path: 'slides',
    loadChildren: () => import('./pages/slides/slides.module').then(m => m.SlidesModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,
      {
    preloadingStrategy: PreloadAllModules,
    relativeLinkResolution: 'legacy'
}
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
