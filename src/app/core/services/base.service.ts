import { HttpClient } from '@angular/common/http';
import { API_URL_TOKEN } from '../constants/injection.constants';
import { HttpGet, GetProvider, HttpPost } from '../decorators/service.decorator';
import { IService } from '../interfaces/service.model';

export class BaseService<TModel> implements IService<TModel> {

    @GetProvider(HttpClient)
    protected http: HttpClient;

    // @GetProvider(API_URL_TOKEN)
    // protected API_URL: string;

    waiting = false;

    constructor() { }

    async get(url: string): Promise<TModel[]> {
        // this.waiting = true;
        // const requestURL = `${this.API_URL}/${url}`;
        // const request = new Promise<TModel[]>((resolve) => {
        //     this.http.get<TModel[]>(requestURL).toPromise()
        //         .then(data => {
        //             resolve(data);
        //         })
        //         .catch(reason => {
        //             this.handlerError(reason);
        //         })
        //         .finally(() => this.waiting = false);
        // });

        // return await request;

        console.log('Get FROM BaseService');
        return Promise.resolve([] as TModel[]);
    }

    async post(url: string, body: TModel): Promise<TModel> {
        console.log('Post FROM BaseService');
        return Promise.resolve({} as TModel);
    }

    handlerError(error: any) {
        console.log(error);
    }

}

export class APIService<TModel> extends BaseService<TModel> {

    constructor() {
        super();
    }


    @HttpGet()
    charge(data?: any): Promise<any> {
        console.log('Charge');
        return Promise.resolve(data || null);
    }

    @HttpPost()
    add(model: any, data?: any) {

    }


}
