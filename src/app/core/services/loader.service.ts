import { Injectable } from "@angular/core";
import { ILoader, ILoaderFactory, ILoaderClient } from "../interfaces/loader.model";
import { TConstructor } from "../interfaces/type";

export class Loader implements ILoader {

    id: number;
    isActive: boolean;

    constructor(instance ? : ILoader) {
        this.id = instance ? instance.id + 1 : 1;
        this.isActive = true;

        if (instance) {
            instance.isActive = false;
        }
    }

}

// @Injectable({ providedIn: 'root' })
// export class LoaderService implements ILoaderClient {

//     private _loaders: ILoader[] = [];

//     constructor(private _factory: ILoaderFactory) { }

//     /**
//      * Add a page loader to the UI
//      * @param opts options passed to factory implementation
//      */
//     async addLoader(opts?: {}): Promise<any> {
//         await this._factory.show(opts);

//         this._loaders.push(new Loader());

//         return Promise.resolve();
//     }

//     /**
//      * Hide a page loader UI when all loaders in the stack are complete
//      */
//     async hideLoader(): Promise<any> {

//         if (this._loaders.length < 1) {
//             return Promise.resolve({ hide: true });
//         }

//         const isCompleteArray = () => this._loaders.filter(x => x.isComplete === false);
//         isCompleteArray()[0].isComplete = true; // Completa uno de los loaders
//         //
//         // Determine if all _loaders are completes, so hide UI loader
//         //
//         const canHideLoader = isCompleteArray().length < 1;

//         if (!canHideLoader) {
// 	    return Promise.resolve({ hide: canHideLoader });
//         }

//         await this.hideAllLoader(); // clear up _loaders and hide UI loader.
//         return Promise.resolve({ hide: canHideLoader });
//     }

//     /**
//      * Hide a pege loader UI, does not matter how many are in the stack, then clear it up.
//      */
//     async hideAllLoader(): Promise<any> {
//         await this._factory.hide(); // Hide UI Loader
//         this._loaders = []; // clear up _loader if all are completes.

//         return Promise.resolve({ hide: true });
//     }

//     loaders() { return this._loaders; }

//     static useFactory<TFactory extends ILoaderFactory>(factory: TConstructor<TFactory>): Function {      
//         return <TService>(service: TService) => {
//             const factoryInstance = new (factory)(service) as ILoaderFactory;
//             return new LoaderService(factoryInstance);
//         }
//     }

// }

@Injectable({ providedIn: 'root' })
export class LoaderService {

    private _onWaiting: number = 0;

    get onWaiting(): number {
        return this._onWaiting;
    }

    _loaderRef: ILoader | undefined;

    constructor(private _factory: ILoaderFactory) {}

    async start(opts ? : any): Promise <ILoader> {
        try {

            console.log("Before Show", this._loaderRef);
            if (!this._loaderRef) {
                await this._factory.show(opts);
            }

            const loader = new Loader(this._loaderRef);
            this._loaderRef = loader;
            this._onWaiting++;

            console.log("Start", this._onWaiting);
            return Promise.resolve(this._loaderRef);

        } catch (e) {
            return Promise.reject("Error Starting Loader\n" + e);
        }
    }

    async stop(): Promise <any> {

        try {
            console.log("Stop", this._onWaiting);
            if (!this._loaderRef) return;

            this._onWaiting--;
            console.log("Less", this._onWaiting);
            if (this._onWaiting > 0) return;

            await this._factory.hide();
            this._loaderRef = undefined;
            this._onWaiting = 0;

            return Promise.resolve();

        } catch (e) {
            return Promise.reject("Error Hiding Loader\n" + e);
        }

    }

    static useFactory < TFactory extends ILoaderFactory > (factory: TConstructor < TFactory > ): Function {
        return <TService > (service: TService) => {
            const factoryInstance = new(factory)(service) as ILoaderFactory;
            return new LoaderService(factoryInstance);
        }
    }

}