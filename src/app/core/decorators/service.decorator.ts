import { APP_ERROR } from "../constants/error.constans";
import { IService } from "../interfaces/service.model";
import { TProvider } from "../interfaces/type";
import { APIService, BaseService } from "../services/base.service";
import { AppInjector } from "../utils/app-injector";

export function GetProvider<T>(token: TProvider<T>): PropertyDecorator {
    return (target: Object, key: string | symbol): void => {

        const service = AppInjector.getInstance(token);

        if (!service) {
            const error = new Error(APP_ERROR.AppProviderNotFound);
            error.name = APP_ERROR.AppProviderNotFoundName;
            throw error;
        }

        Object.defineProperty(target, key, {
            value: service,
            enumerable: true,
            configurable: true
        });

    }
}

/**
 * Llamada de un metodo GET a la API, el metodo al que se coloca
 * debe contener un parametro { DATA } el cual recibe los datos de la llamada a la API
 * 
 * @param path Endpoint de la ruta a la API
 * 
 */
export function HttpGet(path?: string): MethodDecorator {
    return (target: IService<any> & Object, key: string | symbol, descriptor: PropertyDescriptor) => {
        
            if (!(target instanceof BaseService)) {
                throw Error(`${target.constructor.name} is not instanceof BaseService`);
            }

        const originalMethod: Function = descriptor.value;
        const endpoint = path || key;

        descriptor.value = async () => {
            const data = await target.get(String(endpoint));
            originalMethod(data);
        }

        return descriptor;
    }
}

/**
 * Llamada de un metodo POST a la API, el metodo al que se coloca
 * debe contener dos parametros { BODY } cuerpo de la peticion
 * y { DATA } el cual recibe los datos de la llamada a la API
 * 
 * @param path Endpoint de la ruta a la API
 * 
 */
export function HttpPost(path?: string): MethodDecorator {
    return (target: IService<any> & Object, key: string | symbol, descriptor: PropertyDescriptor) => {

        if (!(target instanceof BaseService)) {
            throw Error(`${target.constructor.name} is not instanceof BaseService`);
        }

        const originalMethod: Function = descriptor.value;
        const endpoint = path || key;
        
        descriptor.value = async (body: any) => {

            if (!body) {
                console.warn(`HttpPost was send wihtout body on ${target.constructor.name} method ${String(key)}`);
            }

            const data = await target.post(String(endpoint), body);
            originalMethod(body, data);

        }

        return descriptor;
    }
}
