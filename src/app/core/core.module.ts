import { Injector, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppInjector } from './utils/app-injector';

@NgModule({
    declarations: [],
    imports: [CommonModule],
    exports: [],
    providers: [],
})
export class CoreModule {
    constructor(injector: Injector) {
        AppInjector.setInjector(injector);
    }
}
