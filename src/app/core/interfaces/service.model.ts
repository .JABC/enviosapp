
export interface IService<TModel> {
    get(url: string): Promise<TModel[]>;
    post(url: string, body: TModel): Promise<TModel>;
}

