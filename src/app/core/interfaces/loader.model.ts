export interface ILoader {
    id: number;
    isActive: boolean;
}

export interface ILoaderFactory {
    show(opts?: {}): Promise<any>;
    hide(): Promise<any>;
}

export interface ILoaderClient {
    // _factory: ILoaderFactory;
    addLoader(opts?: {}): Promise<any>;
    hideLoader(): Promise<any>;
    hideAllLoader(): Promise<any>;
}
