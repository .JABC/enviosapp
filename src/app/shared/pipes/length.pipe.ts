import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'length' })
export class LengthPipe implements PipeTransform {
    transform(value: string, limit: number): string {

        if (value.length > limit) {
            value = value.substr(0, limit - 3).concat('...');
        }

        return value;
    }
}