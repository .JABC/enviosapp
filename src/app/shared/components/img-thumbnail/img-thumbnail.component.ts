import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-img-thumbnail',
    templateUrl: './img-thumbnail.component.html'
})
export class ImgThumbnailComponent implements OnInit {

    @Input() dimension: number[] = [90, 90];

    @Input() imageUrl = '';

    @Output() upload = new EventEmitter();

    constructor() { }

    ngOnInit(): void { }

    onUpload() {
        alert('Upload');
    }
}
