import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

export type StarState = 'fill' | 'half' | 'empty';

export interface IStar {
    state: StarState;
}

@Component({
    selector: 'app-rating',
    templateUrl: './rating.component.html'
})
export class RatingComponent implements OnInit, OnChanges {

    @Input() starColor = 'warning';

    @Input() starCount = 5;

    @Input() starSize = '18px';

    @Input() title: string;

    @Input() rating = 2.3;

    @Input() showPercent = true;

    @Input() percentColor = 'secondary';

    @Input() starHalfAfter = 5;

    @Output() action = new EventEmitter();

    ratingPercent: number;

    stars: IStar[] = [];

    constructor() { }

    ngOnInit(): void {
        this.initArray();
        this.calculateRating();
    }

    ngOnChanges(changes: SimpleChanges) {
        const change = changes["rating"];
        if (!change?.isFirstChange() && (change?.previousValue != change?.currentValue)) {
            this.calculateRating();
        }

    }


    calculateRating() {
        const starPercent = (this.rating * 100) / this.starCount;
        this.ratingPercent = starPercent;
        //
        // GetStars: Obtiene el numero de 'stars' en un objeto
        //
        this.getStars(this.rating).then((stars) => {
            for (let i = 0; i < stars.integer; i++) {
                this.stars[i].state = 'fill';

                if (stars?.point) {
                    // i === (stars.integer - 1): La ultima 'star' rellena
                    if (i === (stars.integer - 1) && stars.point > this.starHalfAfter) {
                        this.stars[i + 1].state = 'half';
                    }
                }
            }
        });

    }

    //
    // Llena el array al iniciar con 'stars' vacias para poder iterar en la vista
    //
    private initArray() {
        for (let i = 0; i < this.starCount; i++) {
            this.stars.push({ state: 'empty' });
        }
    }

    //
    // Toma como parametro el rating y los tranforma a un objecto separado
    // para determina las 'stars' enteras y las que son una mitad
    //
    // Integer => numero de 'stars' enteras
    // Point => 'star' por mitad (si es mayor a lo indicado en 'starHalfAfter')
    //
    getStars(decimal: number): Promise<{ integer: number, point?: number }> {
        return new Promise((resolve, reject) => {
            try {
                const splitDecimal = String(decimal).split('.');
                const point = splitDecimal[1]?.charAt(0); // Toma solo el primer decimal

                if (point) {
                    resolve({ integer: Number(splitDecimal[0]), point: Number(point) });
                } else {
                    resolve({ integer: Number(splitDecimal[0]) });
                }

            } catch (e) {
                reject(e);
            }
        });
    }

}

