import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { SLIDEUP_ANIMATION } from '@shared/utils/animations';
// tslint:disable
export interface IComboBox {
    title?: string;
    subtitle?: string;
}

const COMBOBOX_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ComboBoxComponent),
    multi: true
};

@Component({
    selector: 'app-combobox',
    templateUrl: './combobox.component.html',
    animations: [SLIDEUP_ANIMATION],
    providers: [COMBOBOX_VALUE_ACCESSOR]
})
export class ComboBoxComponent implements OnInit, ControlValueAccessor {

    @Input() color = 'primary';

    @Input() title: string;

    @Input() buttonIcon = 'add';

    @Input() backdrop = true;

    @Input() items: IComboBox[] = [
        { title: 'Refresco 200ml', subtitle: 'Coca Cola' },
        { title: 'Refresco 500ml', subtitle: 'Uva' },
        { title: 'Jugo 500ml', subtitle: 'Naranja' }
    ];

    @Output() onSelect = new EventEmitter();

    _value: string;

    set value(value: string) {
        this._value = value;
        this.onChange(this.value);
    }

    get value(): string {
        return this._value;
    }

    onChange: Function = (_: any) => { };

    onTouched: Function = () => { };

    isDisabled: boolean;

    rgb: any;

    canShow = false;

    constructor() { }

    ngOnInit(): void {
        this.getColor();
    }

    getColor(): any {
        const color = getComputedStyle(document.body).getPropertyValue(`--ion-color-${this.color}-rgb`);
        this.rgb = color;
    }

    writeValue(value: string): void {
        this.value = value;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(state: boolean) {
        this.isDisabled = state;
    }

    ok(value: string) {
        this.writeValue(value);
        this.onSelect.emit(value);
        this.toggle();
    }

    toggle() {
        this.canShow = !this.canShow;
    }

}
