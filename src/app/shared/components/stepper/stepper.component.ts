import { Component, OnInit, Input, forwardRef, Output, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
// tslint:disable
const STEPPER_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => StepperComponent),
    multi: true
};

@Component({
    selector: 'app-stepper',
    templateUrl: './stepper.component.html',
    providers: [STEPPER_VALUE_ACCESSOR]
})
export class StepperComponent implements OnInit, ControlValueAccessor {

    @Input() minlength = 0;

    @Input() maxlength: number;

    @Input() step = 1;

    @Input() color = 'primary';

    @Input() plusIcon = 'add-circle-outline';

    @Input() minusIcon = 'remove-circle-outline';

    @Input() iconSize = '16px';

    onChange: Function = (_: any) => { };

    onTouched: Function = () => { };

    isDisabled: boolean;

    _value: number;

    set value(value: number) {
        this._value = value || Number(this.minlength);
        this.onChange(this.value);
    }

    get value(): number {
        return this._value;
    }

    constructor() { }

    ngOnInit(): void { }

    writeValue(value: number): void {
        this.value = value;
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(state: boolean) {
        this.isDisabled = state;
    }

    plus(): void {
        this.value += Number(this.step);
        this.onTouched();
    }

    minus(): void {
        this.value -= Number(this.step);
        this.onTouched();
    }

    get canPlus(): boolean {
        return this._value >= Number(this.maxlength);
    }

    get canMinus(): boolean {
        return this._value <= Number(this.minlength);
    }

}
