import { ImgThumbnailComponent } from './img-thumbnail/img-thumbnail.component';
import { ModalPopupComponent } from './modal-popup/modal-popup.component';
import { RatingComponent } from './rating/rating.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { StepperComponent } from './stepper/stepper.component';

export const COMPONENTS = [
    ImgThumbnailComponent,
    ModalPopupComponent,
    RatingComponent,
    SearchbarComponent,
    StepperComponent
];