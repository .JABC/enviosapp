import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';

export enum TypeModal {
    OkCancel,
    YesOrNo,
    Ok
}

@Component({
    selector: 'app-modal-popup',
    templateUrl: './modal-popup.component.html'
})
export class ModalPopupComponent implements OnInit {

    @Input() title: string;

    @Input() message: string;

    @Input() icon = 'warning-outline';

    @Input() type: TypeModal = TypeModal.YesOrNo;

    buttons = [];

    constructor(public modal: ModalController) { }

    ngOnInit(): void {
        this.fillButtons();
    }

    private fillButtons() {
        switch (this.type) {
            case TypeModal.YesOrNo:
                this.buttons = [
                    {
                        label: 'Si',
                        listener: () => this.modal.dismiss({ dismissed: false }),
                        style: '',
                        color: 'primary',
                        fill: 'solid'
                    },
                    {
                        label: 'No',
                        listener: () => this.modal.dismiss({ dismissed: true }),
                        style: 'border: 1px solid #dddce1;',
                        color: 'dark',
                        fill: 'clear'
                    }
                ];
                break;
            case TypeModal.Ok:
                this.buttons = [
                    {
                        label: 'Aceptar',
                        listener: () => this.modal.dismiss({ dismissed: false }),
                        style: '',
                        color: 'primary',
                        fill: 'solid'
                    }
                ];
                break;

            case TypeModal.OkCancel:
                this.buttons = [
                    {
                        label: 'Aceptar',
                        listener: () => this.modal.dismiss({ dismissed: false }),
                        style: '',
                        color: 'primary',
                        fill: 'solid'
                    },
                    {
                        label: 'Cancelar',
                        listener: () => this.modal.dismiss({ dismissed: true }),
                        style: 'border: 1px solid #dddce1;',
                        color: 'dark',
                        fill: 'clear'
                    }
                ];
                break;

            default:
                console.error(`Type ${TypeModal[this.type]} does not implements`);
                break;
        }
    }

}