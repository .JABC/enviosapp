import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule, LoadingController } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LengthPipe } from './pipes/length.pipe';
import { COMPONENTS } from './components';
import { TEMPLATES } from './templates';
import { DateAgoPipe } from './pipes/time.pipe';
import { ShowWhenDirective } from './directives/showWhen.directive';
import { FilterPipe } from './pipes/filter.pipe';

@NgModule({
    declarations: [
        ...COMPONENTS,
        ...TEMPLATES,
        LengthPipe,
        DateAgoPipe,
        FilterPipe,
        ShowWhenDirective
    ],
    imports: [
        CommonModule,
        IonicModule.forRoot(),
        FormsModule,
        HttpClientModule
    ],
    exports: [
        IonicModule,
        FormsModule,
        HttpClientModule,
        ...COMPONENTS,
        ...TEMPLATES,
        LengthPipe,
        DateAgoPipe,
        FilterPipe,
        ShowWhenDirective
    ],
    providers: [],
})
export class SharedModule { }
