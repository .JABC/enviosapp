import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { EShopState } from '@pages/tienda/tienda.model';

@Component({
    // tslint:disable-next-line
    selector: 'restaurante-item',
    templateUrl: './restaurante-item.component.html'
})
export class RestauranteItemComponent implements OnInit {

    SHOPSTATE = EShopState;

    @Input() name: string;

    @Input() thumbnail: string;

    @Input() isNew: boolean;

    @Input() isNewColor: string = 'danger';

    @Input() state: number = 3;

    @Input() stateTextColor = 'danger';

    @Input() rating: string | number;

    @Input() delivery: string;

    @Input() minPrice: string | number;

    @Input() acceptCards;

    constructor() { }

    ngOnInit(): void { }
}
