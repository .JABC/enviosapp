import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
    selector: 'card-doble',
    templateUrl: 'card-doble.component.html'
})

export class CardDobleComponent implements OnInit {
    
    @HostBinding('style.width') width = '100%';

    @Input() title = 'Burger King';

    @Input() rating = 4.2;

    @Input() imgURL = 'assets/img/burger-king-food.jpg';

    @Input() imgAlt: string;

    @Input() time = '10 min';

    @Input() payment = 'online';

    @Input() delivery = 'Gratis';

    constructor() { }

    ngOnInit() { }
}