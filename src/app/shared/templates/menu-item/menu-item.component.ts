import { Component, Input, OnInit } from '@angular/core';
import { EMenuState } from '@pages/tienda/menu/menu.model';

@Component({
    // tslint:disable-next-line
    selector: 'menu-item',
    templateUrl: './menu-item.component.html'
})
export class MenuItemComponent implements OnInit {

    MENUSTATE = EMenuState;

    @Input() name: string;

    @Input() price: number;

    @Input() details: string;

    @Input() thumbnail: string;

    @Input() state: number;

    constructor() { }

    ngOnInit(): void { }
}
