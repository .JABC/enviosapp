import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-categoria-item',
    templateUrl: './categoria-item.component.html'
})
export class CategoriaItemComponent implements OnInit {

    @Input() icon = 'assets/img/food.svg';

    @Input() iconSize = '24px';

    @Input() title = 'Pizza';

    @Input() subtitle = '25 Locales';

    constructor() { }

    ngOnInit(): void { }
}
