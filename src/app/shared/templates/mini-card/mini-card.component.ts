import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-mini-card',
    templateUrl: './mini-card.component.html'
})
export class MiniCardComponent implements OnInit {

    @Input() title: string;

    @Input() imgURL: string;

    @Input() imgAlt: string;

    @Input() subtitle: string;

    @Input() rating: number | string;

    @Input() time: string;

    @Input() price: string;

    constructor() { }

    ngOnInit(): void { }
}
