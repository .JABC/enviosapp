import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-pedido-item',
    templateUrl: './pedido-item.component.html'
})
export class PedidoItemComponent implements OnInit {

    @Input() name: string;

    @Input() price: number;

    @Input() unit: number;

    @Input() description: string;

    @Input() actions = true;

    @Output() delete = new EventEmitter();

    @Output() edit = new EventEmitter();

    constructor() { }

    ngOnInit(): void { }
}
