import { BuscarItemComponent } from "./buscar-item/buscar-item.component";
import { CardDetailComponent } from "./card-detail/card-detail.component";
import { CardDobleComponent } from "./card-doble/card-doble.component";
import { CategoriaItemComponent } from "./categoria-item/categoria-item.component";
import { MenuItemComponent } from "./menu-item/menu-item.component";
import { MenuOfferComponent } from "./menu-offer/menu-offer.component";
import { MiniCardComponent } from "./mini-card/mini-card.component";
import { PedidoItemComponent } from "./pedido-item/pedido-item.component";
import { RestauranteItemComponent } from "./restaurante-item/restaurante-item.component";


export const TEMPLATES = [
    BuscarItemComponent,
    CardDetailComponent,
    CategoriaItemComponent,
    MenuItemComponent,
    MenuOfferComponent,
    MiniCardComponent,
    PedidoItemComponent,
    RestauranteItemComponent,
    CardDobleComponent
];

