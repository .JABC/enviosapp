import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
    // tslint:disable-next-line
    selector: 'menu-offer',
    templateUrl: './menu-offer.component.html'
})
export class MenuOfferComponent implements OnInit {

    @HostBinding('style.width') width = '100%';

    @Input() title: string = "Combo Mini";

    @Input() imageURL: string = "assets/img/burger-king-food.jpg";

    @Input() imgAlt: string;

    @Input() thumbnail: string = "assets/img/burger-king-logo.png";

    @Input() details: string = "Combo de papas fritas y refresco pequeños";

    constructor() { }

    ngOnInit(): void { }
}
