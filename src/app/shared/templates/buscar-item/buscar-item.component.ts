import { Component, Input, OnInit } from '@angular/core';
import { EShopState } from '@pages/tienda/tienda.model';

@Component({
    selector: 'app-buscar-item',
    templateUrl: './buscar-item.component.html'
})
export class BuscarItemComponent implements OnInit {

    SHOPSTATE = EShopState;

    @Input() thumbnail: string = "assets/img/burger-king-logo.png";

    @Input() name: string = "Name";

    @Input() rating: number | string = 1.2;

    @Input() minPrice: number = 89;

    @Input() delivery: string = "Gratis";

    @Input() state: number;

    @Input() stateTextColor: string = "danger";

    constructor() { }

    ngOnInit(): void { }
}
