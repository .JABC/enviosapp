import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-card-detail',
    templateUrl: './card-detail.component.html'
})
export class CardDetailComponent implements OnInit {

    @Input() title = 'Burger King';

    @Input() rating = 4.2;

    @Input() imgURL = 'assets/img/burger-king-food.jpg';

    @Input() imgAlt: string;

    @Input() buttonLabel = 'Ordenar';

    @Input() note = '20 - 30 min';

    @Input() thumbnail = 'assets/img/burger-king-logo.png';

    @Input() categories: any[];

    @Output() action = new EventEmitter();

    constructor() { }

    ngOnInit(): void { }
}
