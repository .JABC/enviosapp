import { Directive, OnInit, Input, ViewContainerRef, TemplateRef, EventEmitter, Output } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { distinctUntilChanged, map, startWith, takeUntil } from 'rxjs/operators';
import { DestroyService } from 'src/app/core/services/destroy.service';

@Directive({
    selector: '[showWhen]',
    exportAs: 'showWhen',
    providers: [DestroyService]
})
export class ShowWhenDirective implements OnInit {

    @Input()
    showWhen: number;

    @Input()
    container: IonContent;

    @Input()
    attrName: string = "data-show";

    @Output()
    onToggle = new EventEmitter();

    private _hasShow: boolean;

    set hasShow(v: boolean) {
        this._hasShow = v;
        this._hasShow
            ? this.viewContainer.createEmbeddedView(this.template)
            : this.viewContainer.clear();
    }

    get hasShow(): boolean {
        return this._hasShow;
    }

    // Component has been forced to show when it's opened by toggle function
    // instead of by scroll, only toggle function can close it again.
    hasBeenForced: boolean = false;

    constructor(
        private readonly viewContainer: ViewContainerRef,
        private readonly template: TemplateRef<unknown>,
        private readonly destroy$: DestroyService
    ) { }

    ngOnInit() {
        this.container.ionScroll.pipe(
            takeUntil(this.destroy$),
            map(ev => ev.detail?.scrollTop),
            map(top => this.showWhen ? top > this.showWhen : top > 200),
            distinctUntilChanged(),
            startWith(this.showWhen === 0)
        ).subscribe(show => {
            if (!this.hasBeenForced) {
                this.hasShow = show;
            }
        })
    }

    toggle(show?: boolean) {
        show
            ? this.hasShow = show
            : this.hasShow = !this._hasShow;

        this.hasBeenForced = this._hasShow;
        this.onToggle.emit(this._hasShow);
    }

}